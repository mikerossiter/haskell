eulerNetwork :: Int -> Int -> Int -> String
eulerNetwork v r l
    | v + r - l == 1 = "This is valid"
    | otherwise = "Invalid network"
